class User < ActiveRecord::Base
	before_save { self.msv = msv.downcase }
	validates :name, presence: true, length: { maximum: 50}
	validates :maSV, presence: true, length: { maximum: 20}, 
					uniqueness: {case_sensitive: false}
	has_secure_password
	#validates :password, presence: true, length: { minimum: 6 }
end
