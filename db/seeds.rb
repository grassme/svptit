User.create!(name:  "Huyen Lanh",
             msv: "B14DCCN015",
             password_digest: "12345678",
             email: "lalala@gmail.com",
             sdt: "0989390159",
             address: "Van Quan, Ha Dong",
             admin:     true)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@gmail.com"
  password = "12345678"
  msv = "B14DCCN#{n+1}"
  User.create!(name: name,
              msv: msv,
              password:              password,
              password_confirmation: password),
              email: email
end