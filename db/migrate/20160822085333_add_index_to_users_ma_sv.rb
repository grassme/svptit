class AddIndexToUsersMaSv < ActiveRecord::Migration
  def change
  	add_index :users, :msv, unique: true
  end
end
